### The JS Frameworks and Libraries for the programme are

<b>Cryptography :- 

List of JavaScript Crypto libraries. 

https://gist.github.com/jo/8619441

https://tweetnacl.js.org/#/

https://github.com/asmcrypto/asmcrypto.js

https://github.com/jedisct1/libsodium.js

<b>Web Stack :-

Front End : ReactJS, Angular, VueJS, ExpressJS, MeteorJS, EmberJS

https://github.com/collections/front-end-javascript-frameworks

BackEnd : NodeJS

Some survey data of JS frameworks
https://stateofjs.com/2017/back-end/results/

<b>Virtual Reality:-

WebVR - A-frame
WebGL - BabylonJS, ThreeJS

<b>Augmented Web :-

AR.js and ArgonJS

<b>Progressive Web Apps :-

Any frontend framework App UI can be used.

<b>Responsive Web Design :-

There are CSS frameworks and CSS Boilerplates. We can use whichever is needed based on time constraints.

Bootstrap, Vanilla, Avalanche

And others are listed in below articles:

https://speckyboy.com/responsive-lightweight-css-frameworks/

https://www.awwwards.com/what-are-frameworks-22-best-responsive-css-frameworks-for-web-design.html

<b>Data Extraction 

SiphonJS (https://github.com/siphonjs/siphon)

https://medium.com/@george.norberg/web-scraping-at-scale-with-javascript-and-node-js-a716fe2208f4

<b>Data Analytics :-

D3JS, DataSet, TableTop, jStat

More Libraries

http://jgoodall.me/2012/02/01/javascript-statistical-libraries.html

https://www.computerworld.com/article/2495888/web-apps/web-apps-six-useful-javascript-libraries-for-dealing-with-data.html

<b>Data Visualization :-

Data Visualization with JS - A book

http://jsdatav.is/intro.html

Some Libraries: D3JS, HighCharts, Protochart, Bluff

https://www.webpagefx.com/blog/web-design/20-fresh-javascript-data-visualization-libraries/

